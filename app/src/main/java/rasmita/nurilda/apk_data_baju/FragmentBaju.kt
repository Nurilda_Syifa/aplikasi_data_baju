package rasmita.nurilda.apk_data_baju

import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_baju.*
import kotlinx.android.synthetic.main.frag_data_baju.view.*

class FragmentBaju : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    lateinit var thisParent : MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaBaju : String=""
    var namaUkuran : String=""
    lateinit var db : SQLiteDatabase
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDelete ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukan sudah benar?")
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdate ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnInsert ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        namaUkuran = c.getString(c.getColumnIndex("_id"))
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        v.edKode.setText(c.getString(c.getColumnIndex("_id")))
        v.edNama.setText(c.getString(c.getColumnIndex("nama_baju")))
        v.edStok.setText(c.getString(c.getColumnIndex("stok")))
        v.spinner.setSelection(getIndex(v.spinner,namaUkuran))

    }

    fun getIndex(spinner: Spinner, myString: String): Int {
        var a = spinner.count
        var b : String = ""
        for (i in 0 until a) {
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_baju,container, false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDelete.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnUpdate.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.lsBaju.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataBaju("")
        showDataUkuran()
    }


    fun showDataBaju(namaBaju: String){
        var sql = ""
        if(!namaBaju.trim().equals("")){
            sql = "select b.kode as _id, b.nama_baju, b.stok, u.nama_ukuran from baju b, ukuran u"+
                    "where b.id_ukuran = u.id_ukuran and b.nama_baju like '%$namaBaju%'"
        }else{
            sql = "select b.kode as _id, b.nama_baju, b.stok, u.nama_ukuran from baju b, ukuran u "+
                    "where b.id_ukuran = u.id_ukuran order by b.nama_baju asc"
        }
        val c : Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_baju,c,
        arrayOf("_id","nama_baju","stok","nama_ukuran"), intArrayOf(R.id.txtKode, R.id.txtNama, R.id.txtStok, R.id.txtUkuran),
        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsBaju.adapter = lsAdapter
    }

    fun showDataUkuran(){
        val c : Cursor = db.rawQuery("select nama_ukuran as _id from ukuran order by nama_ukuran asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }


    fun insertDataBaju(kode : String, namaBaju : String, stok : String, id_ukuran: Int){
        var sql = "insert into baju(kode, nama_baju, stok, id_ukuran) values (?,?,?,?)"
        db.execSQL(sql, arrayOf(kode, namaBaju, stok, id_ukuran))
        showDataBaju("")
    }

    fun updateDataBaju(namaBaju: String, stok: String, id_ukuran: Int, kode : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_baju",namaBaju)
        cv.put("stok",stok)
        cv.put("id_ukuran",id_ukuran)
        db.update("baju",cv,"Kode = $kode",null)
        showDataBaju("")
    }

    fun deleteDataBaju(kode: String) {
        db.delete("baju", "Kode = $kode", null)
        showDataBaju("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql =  " select id_ukuran from ukuran where nama_ukuran ='$namaUkuran'"
        val c : Cursor =  db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            insertDataBaju(v.edKode.text.toString(), v.edNama.text.toString(), v.edStok.text.toString(),
                c.getInt(c.getColumnIndex("id_ukuran")))
            v.edKode.setText("")
            v.edNama.setText("")
            v.edStok.setText("")
        }
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql =  " select id_ukuran from ukuran where nama_ukuran ='$namaUkuran'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            updateDataBaju( v.edNama.text.toString(), v.edStok.text.toString(),
                c.getInt(c.getColumnIndex("id_ukuran")), "kode")
            v.edKode.setText("")
            v.edNama.setText("")
            v.edStok.setText("")
        }
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataBaju("kode")
        v.edNama.setText("")
        v.spinner.setSelection(0)
        v.edStok.setText("")
    }
}