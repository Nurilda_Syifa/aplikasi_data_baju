package rasmita.nurilda.apk_data_baju

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    lateinit var db : SQLiteDatabase
    lateinit var fragBaju : FragmentBaju
    lateinit var fragHarga : FragmentHarga
    lateinit var fragMerek : FragmentMerek
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragBaju =  FragmentBaju()
        fragHarga = FragmentHarga()
        fragMerek = FragmentMerek()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject(): SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemBaju ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragBaju).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.ItemInfo -> frameLayout.visibility = View.GONE

            R.id.itemHarga ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragHarga).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,225,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMerek ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMerek).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,225,255))
                frameLayout.visibility = View.VISIBLE
            }
        }
        return true
    }
}
