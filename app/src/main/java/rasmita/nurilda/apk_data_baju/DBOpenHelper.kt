package rasmita.nurilda.apk_data_baju

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context): SQLiteOpenHelper(context, DB_Name, null, DB_Ver) {
    companion object{
        val DB_Name = "baju"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tBaju = "create table baju(kode text primary key, nama_baju text not null, stok text not null, id_ukuran int not null)"
        val tUkuran = "create table ukuran(id_ukuran integer primary key autoincrement, nama_ukuran text not null)"
        val insUkuran = "insert into ukuran(nama_ukuran) values('S'),('M'),('L'),('XL'),('XXL'),('All Size')"
        val tHarga = "create table harga(id_harga integer primary key autoincrement , kode text not null,id_merek integer not null , nama_harga text not null)"
        val tMerek = "create table merek(id_merek integer primary key autoincrement , nama_merek text not null)"
        val insMerek = "insert into merek(nama_merek) values('Nevada'),('Yongki Komaladi'),('Logo'),('Conextion'),('Phenomenal'),('ada')"
        db?.execSQL(tBaju)
        db?.execSQL(tUkuran)
        db?.execSQL(insUkuran)
        db?.execSQL(tHarga)
        db?.execSQL(tMerek)
        db?.execSQL(insMerek)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}