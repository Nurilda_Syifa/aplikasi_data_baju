package rasmita.nurilda.apk_data_baju

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_harga.*
import kotlinx.android.synthetic.main.frag_data_harga.view.*

class FragmentHarga : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertHarga->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var Adapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter1 : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var namaBaju : String = ""
    var namaMerek : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_harga,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnInsertHarga.setOnClickListener(this)
        v.spBaju.onItemSelectedListener = this
        v.spMerek.onItemSelectedListener = this
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataHarga()
        showDataBaju()
        showDataMerek()
    }

    fun showDataHarga(){
        var sql= "select harga.id_harga as _id, baju.nama_baju, merek.nama_merek, harga.nama_harga from harga join baju on harga.kode = baju.kode join merek on harga.kode order by harga.id_harga asc"
        val c : Cursor = db.rawQuery(sql,null)
        Adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_harga,c,
        arrayOf("_id","nama_baju","nama_merek","nama_harga"), intArrayOf(R.id.txIdHarga,R.id.txNamaMerek,R.id.txNamaBaju,R.id.txHarga),
        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
    }
    fun showDataBaju(){
        val c : Cursor = db.rawQuery("select nama_baju as _id from baju order by nama_baju asc", null)
        spAdapter1 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
        arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spBaju.adapter = spAdapter1
        v.spBaju.setSelection(0)
    }

    fun showDataMerek(){
        val c1 : Cursor = db.rawQuery("select nama_merek as _id from merek order by nama_merek asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c1,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMerek.adapter = spAdapter
        v.spMerek.setSelection(0)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spBaju.setSelection(0,true)
        spMerek.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter1.getItem(position) as Cursor
        val c1 = spAdapter.getItem(position) as Cursor
        namaBaju = c1.getString(c.getColumnIndex("_id"))
        namaMerek = c.getString(c.getColumnIndex("_id"))
    }

    fun insertDataHarga(nama : String,nBaju : String, harga : String){
        var sql = "insert into harga(kode,id_merek, harga) values (?,?,?)"
        db.execSQL(sql, arrayOf(nama,nBaju,harga))
        showDataHarga()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select kode from baju where nama_baju='$namaBaju'"
        var sql1 = "select id_merek from merek where nama_merek='$namaMerek'"
        var c : Cursor = db.rawQuery(sql,null)
        var c1 : Cursor = db.rawQuery(sql1,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataHarga(c.getString(c.getColumnIndex("kode")),c1.getString(c1.getColumnIndex("id_merek")),v.edHarga.text.toString())
            v.edHarga.setText("")
        }
        var x = v.edHarga.text.toString()
        v.txVHarga.setText("$namaBaju, $namaMerek,$x")
        v.edHarga.setText("")
    }
}