package rasmita.nurilda.apk_data_baju

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.appcompat.widget.DecorContentParent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.frag_data_merek.view.*

class FragmentMerek : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteMerek ->{
                builder.setTitle("Konfirmasi").setMessage("Data Yang Dimasukkan Sudah Benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnUpdateMerek ->{
                builder.setTitle("Konfirmasi").setMessage("Data Yang Dimasukkan Sudah Benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()

            }
            R.id.btnInsertMerek ->{
                builder.setTitle("Konfirmasi").setMessage("Data Yang Dimasukkan Sudah Benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()

            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idMerek : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_data_merek,container,false)
        v.btnDeleteMerek.setOnClickListener(this)
        v.btnUpdateMerek.setOnClickListener(this)
        v.btnInsertMerek.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsMerek.setOnItemClickListener(itemClick)

        return v
    }
    fun showDataMerek(){
        val cursor : Cursor = db.query("merek", arrayOf("nama_merek","id_merek as _id"),
        null, null, null, null, "nama_merek asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_datamerek,cursor,
        arrayOf("_id","nama_merek"), intArrayOf(R.id.txIdMerek, R.id.txNamaMerek),
        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMerek.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataMerek()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idMerek = c.getString(c.getColumnIndex("_id"))
        v.edMerekB.setText(c.getString(c.getColumnIndex("nama_merek")))
    }

    fun insertDataMerek(namaMerek: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_merek",namaMerek)
        db.insert("merek", null,cv)
        showDataMerek()
    }

    fun updateDataMerek(namaMerek: String, idMerek: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_merek",namaMerek)
        db.update("merek",cv, "id_merek = $idMerek", null)
        showDataMerek()
    }

    fun deleteDataMerek(idMerek: String){
        db.delete("merek", "id_merek = $idMerek", null)
        showDataMerek()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMerek(v.edMerekB.text.toString())
        v.edMerekB.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataMerek(v.edMerekB.text.toString(),idMerek)
        v.edMerekB.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMerek(idMerek)
        v.edMerekB.setText("")
    }

}